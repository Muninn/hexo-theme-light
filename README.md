# Light

Default theme for [Hexo](https://hexo.io).

With my edits and changes for the personal blog I run: http://www.dougbromley.com

See the original to fork or install: https://github.com/hexojs/hexo-theme-light
